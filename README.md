<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# System Update

Ansible role that updates all system packages and globally installed NPM packages.

- Updates all APT packages to the lastest version available.
- Updates all NPM packages installed globally.

## 🦺 Requirements

- For NPM packges you should have `npm` installed (duh!)

## 🗃️ Role Variables

*None*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
